<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



	public function create(request $r){

        $roles=[]; 

        if(isset($r['role'])){

        	foreach ($r['role'] as $role) {
	        	$roles[] = Role::where('name','=',$role)->first();	
	        }	
        }
        
        
       
        $permission = new Permission();
        $permission->name = $r->permission_title;
        $permission->save();


        if(!empty($roles)){
        	foreach ($roles as $role) {
	        	$permission->roles()->attach($role->id);
	        }	
        }
        
        
        return redirect()->route('permission.create')->with('info' ,'new type of permission has been created');
    }



    public function assignToRoles(Request $r){

    	$permission = Permission::where('name','=',$r['permission-name'])->first();

    	foreach ($r['role'] as $k) {
    		$role = Role::where('name','=',$k)->first();

    		$permission->roles()->attach($role->id);
    	}

    	$permission->save();

    	return redirect()->back()->with('info', $permission->name. ' permission has been assigned to the roles you selected');
    }




    public function revokeRoles(Request $r){

    	$permission = Permission::where('name','=',$r['permission-name'])->first();

    	foreach ($r['role'] as $k) {
    		$role = Role::where('name','=',$k)->first();

    		$permission->roles()->detach($role->id);
    	}

    	$permission->save();

    	return redirect()->back()->with('info', $permission->name. ' permission has been revoked from the roles you selected');

    }



    /**
     * Show the pages
     */

    public function getCreatePage(Request $r){

        $roles = Role::all()->except(['1']);
        return view('permission.permission_create',compact('roles'));
    }

    public function getEditPage(){

    	$roles = Role::all()->except(['1']);
    	$permissions = Permission::with('roles')->get();

    	return view('permission.permission_edit',compact('permissions','roles'));
    	
    }
}
