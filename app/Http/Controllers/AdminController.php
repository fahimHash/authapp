<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Role;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin');
    }



    public function create(request $r){

        $role = Role::where('name','=',$r->role)->first();

        $admin = new Admin();
        $admin->name = $r->login_id;
        $admin->email = $r->email;
        $admin->password = bcrypt($r->password);
        $admin->save();
        $admin->roles()->attach($role->id);


        return redirect()->route('admin.create')->with('info' ,'admin has been created');
    }

    public function ban(Request $r,$admin){

        $ad = Admin::where('name','=',$admin)->first();
        $ad->active = 0;
        $ad->save();

        return redirect()->back()->with('info' ,$admin.'\'s account has been banned');   
    }

    public function activate(Request $r,$admin){

        $ad = Admin::where('name','=',$admin)->first();
        $ad->active = 1;
        $ad->save();

        return redirect()->back()->with('info' ,$admin.'\'s account has been activated');
    }

    /**
     * Show the admin pages
     */

    public function getCreatePage(Request $r){

        $roles = Role::all()->except(['1']);
        return view('admin.admin_create',compact('roles'));
    }

    public function getListPage(){
        $admins = Admin::with('roles')->get();
        // dd($admins[0]->roles);
        return view('admin.admin_list',compact('admins'));
    }

    public function getProfilePage($admin){
        $ad = Admin::where('name','=',$admin)->first();

        return view('admin.profile',compact('ad'));
        
    }
}
