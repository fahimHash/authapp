<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller
{

	public function __construct(){

		$this->middleware('guest:admin');

	}

    public function showLoginForm(Request $r){

    	return view('auth.admin-login');
    	
    }

    public function login(Request $r){
    	//validate

    	//login
    	if(Auth::guard('admin')->attempt(['email' => $r->email, 'password' => $r->password],$r->remember))
    	{
    		return redirect()->route('admin.dashboard');
    	}

    	return redirect()->back($r->only('email'));
    }
}
