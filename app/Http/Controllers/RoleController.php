<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



	public function create(request $r){
        $role = new Role();
        $role->name = $r->role_title;
        $role->save();

        return redirect()->back()->with('info','New role has been created');
    }


    public function list(Request $r){
        $roles = Role::with('permissions')->get();
        return view('role.role_list',compact('roles'));
    }

    /**
     * Show the pages
     */

    public function getCreatePage(Request $r){

        return view('role.role_create');
    }

}
