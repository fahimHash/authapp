<?php

namespace App;

use App\Role;
use App\Admin;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

	public $timestamps = false;

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function admins()
    {
        return $this->belongsToMany(Admin::class);
    }

    public function unassignedRoles()
    {
    	$roles = $this->roles;
    	$allRoles = Role::where('name','!=','su')->get();

    	return $allRoles->diff($roles);
    	
    }
}
