<?php

namespace App;

use App\Admin;
use App\Permission;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public $timestamps = false;

    public function admins()
    {
        return $this->belongsToMany(Admin::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
