
@extends('layouts.app')

@section('style_sheet')

<link href="{{ URL::asset('/css/admin.css') }}" rel="stylesheet">

@endsection

@section('content')

    @if(Session::has('info'))
    <div class="container-fluid">
        <div class="alert alert-info" role="alert" style="text-align: center;">
            {{Session::get('info')}}
        </div>
    </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12" id="dashboard-container">
                <div id="dash">
                    <h4><a href="{{route('admin.dashboard')}}">Dashboard</a></h4>
                </div>

                <div id="dash-admin-name">
                    <h4><i class="fa fa-user" aria-hidden="true"></i><span class="give-space"></span><a href="#">{{ Auth::user()->name }}</a> <span><strong>({{Auth::user()->roles[0]->name}})</strong></span></h4>
                </div>
                
                <div id="dash-admin-notification">
                    <h4><i class="fa fa-envelope" aria-hidden="true"></i><span class="give-space"></span><a href="#">messages</a></h4>
                </div>

                <div id="dash-admin-editProfile">
                    <h4><i class="fa fa-pencil-square-o" aria-hidden="true"></i><span class="give-space"></span><a href="#">edit profile</a></h4>
                </div>            
            </div> 
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12" id="admin-container">
                <div id="menu-box">

                    <div class="menu-box-item">
                        <h3>Admin</h3>
                        <ul>
                            <li><a href="{{route('admin.create')}}">New Admin</a></li>
                            <li><a href="{{route('admin.list')}}">List Admins</a></li>
                        </ul>
                    </div>

                    <div class="menu-box-item">
                        <h3>Role</h3>
                        <ul>
                            <li><a href="{{route('role.create')}}">New Role</a></li>
                            <li><a href="{{route('role.list')}}">List Roles</a></li>
                        </ul>
                    </div>

                    <div class="menu-box-item">
                        <h3>Permission</h3>
                        <ul>
                            <li><a href="{{route('permission.create')}}">New Permission</a></li>
                            <li><a href="{{route('permission.edit')}}">Edit</a></li>
                        </ul>
                    </div>
                </div>

                <div id="content-box">
                    @yield('admin-content-box')
                </div>
            </div>
        </div>
    </div>

@endsection
