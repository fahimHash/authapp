@extends('admin')



@section('admin-content-box')
	<h3>New Role</h3>
	<hr>
	<form class="" method="post" action="{{route('role.create')}}">

	  <div class="form-group">
	    <label for="username">Role Title</label>
	    <input type="text" name="role_title" class="form-control" id="role_title" 
	    	   placeholder="role title"
	    >
	  </div>

	  {{ csrf_field() }}
	  <input type="submit" class="btn btn-default" value="Create">
	</form> 


@endsection
