@extends('admin')


@section('admin-content-box')
	
	<h3>List Of Roles</h3>
	<hr>


	<table class="table table-hover table-bordered table-striped" id="admin-edit-table">
		<thead> 
			<tr> 
				<th>role title</th> 
				<th>permissions</th> 
				<th>admins with roles</th>  
			</tr> 
		</thead>

		<tbody>
			@foreach($roles as $role)
				
				<tr>
					<td>{{$role->name}}</td>

					<td>
					@foreach($role->permissions as $p)	
						{{$p->name}},
					@endforeach	
					</td>

					<td>
					@foreach($role->admins as $a)	
						<a href="#">{{$a->name}}</a>,
					@endforeach
					</td>	
				</tr>
				
			@endforeach
		</tbody>
	</table>

@endsection
