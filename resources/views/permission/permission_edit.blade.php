@extends('admin')

<div class="modal bs-example-modal-lg fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Attach</h4>
      </div>
      <div class="modal-body">
        
      	<form method="post" id="message-form" action="{{route('permission.roles.assign')}}">

      	  <div class="form-group">
	    
		  		<label>Attach to roles</label>
		  		
		  		<div class="checkbox" id="form-check-role">
				  
				</div> 

		  </div>

		  <input id="hidden-perm-name" type="hidden" name="permission-name" value="">
		  
		  <button type="submit" class="btn btn-success">Attach</button>
		  {{csrf_field()}}
		</form>

      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>




<div class="modal bs-example-modal-lg fade" id="revokeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Revoke Permission From</h4>
      </div>
      <div class="modal-body">
        
      	<form method="post" id="revoke-form" action="{{route('permission.roles.revoke')}}">

      	  <div class="form-group">
	    
		  		<label>Roles</label>
		  		
		  		<div class="checkbox" id="form-check-revoke-role">
				  
				</div> 

		  </div>

		  <input id="hidden-perm-name" type="hidden" name="permission-name" value="">
		  
		  <button type="submit" class="btn btn-danger">Revoke</button>
		  {{csrf_field()}}
		</form>

      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

@section('admin-content-box')
	
	<h3>All Permissions</h3>
	<hr>


	<table class="table table-hover table-bordered table-striped" id="admin-edit-table">
		<thead> 
			<tr> 
				<th>title</th> 
				<th>roles</th> 
				<th>attach to roles</th> 
				<th>revoke from roles</th>  
			</tr> 
		</thead>

		<tbody>
			@foreach($permissions as $p)
				
				<tr>
					<td>{{$p->name}}</td>
					<td>
						@foreach($p->roles as $r)
							{{$r->name}},
						@endforeach
					</td>

					
					<td><a class="btn btn-block btn-success assign-btn" href="" data-name="{{$p->name}}"
						   data-roles-to-assign="{{$p->unassignedRoles()}}" 
						   data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></td>

					
					<td><a class="btn btn-block btn-danger revoke-btn" href="" data-name="{{$p->name}}"
						   data-roles-to-revoke="{{$p->roles}}"	
					       data-toggle="modal" data-target="#revokeModal"><i class="fa fa-times" aria-hidden="true"></i></a></td>
					

				</tr>
				
			@endforeach
		</tbody>
	</table>

@endsection

@section('scripts')

	<script type="text/javascript">

	    $(document).ready(function(){
	        
	        //what happens if assign button is clicked
	    	$('.assign-btn').click(function(){
	    		$('#message-form #hidden-perm-name').val($(this).data('name'));

	    		var data = $(this).data('roles-to-assign');
	    		console.log(data);
	    		$('#form-check-role').empty();

	    		for(i=0;i<data.length;i++){

	    			var start = "<label><input type='checkbox' value=";
	    			var mid01 = "'"+data[i].name+"'"; 
	    			var mid02 = " name='role[]'>";
	    			var end   = data[i].name + "</label>"+"<br>";
	    			
	    			var el = start+mid01+mid02+end;
	    			console.log(el);
	    			$('#form-check-role').append(el);

	    		}

	    	});


	    	//what happens if revoke button is clicked
	    	$('.revoke-btn').click(function(){
	    		$('#revoke-form #hidden-perm-name').val($(this).data('name'));

	    		var data = $(this).data('roles-to-revoke');
	    		console.log(data);
	    		$('#form-check-revoke-role').empty();

	    		for(i=0;i<data.length;i++){

	    			var start = "<label><input type='checkbox' value=";
	    			var mid01 = "'"+data[i].name+"'"; 
	    			var mid02 = " name='role[]'>";
	    			var end   = data[i].name + "</label>"+"<br>";
	    			
	    			var el = start+mid01+mid02+end;
	    			console.log(el);
	    			$('#form-check-revoke-role').append(el);

	    		}

	    	});
	    	//.............

	    });

	</script>

@endsection
