@extends('admin')



@section('admin-content-box')
	<h3>New Permission</h3>
	<hr>
	<form class="" method="post" action="{{route('permission.create')}}">

	  <div class="form-group">
	    <label for="username">permission title</label>
	    <input type="text" name="permission_title" class="form-control" id="permission_title" 
	    	   placeholder="permission title"
	    >
	  </div>

	  <div class="form-group">
	    
	  		<label>assign to roles</label>
	  		@foreach($roles as $role)
	  		<div class="checkbox">
			  <label>
			    <input type="checkbox" value="{{$role->name}}" name="role[]">
			    {{$role->name}}
			  </label>
			</div> 
	  		@endforeach

	  </div>
	  {{ csrf_field() }}
	  <input type="submit" class="btn btn-default" value="Create">
	</form> 


@endsection
