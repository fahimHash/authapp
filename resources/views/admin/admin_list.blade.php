@extends('admin')

<div class="modal bs-example-modal-lg fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Compose message</h4>
      </div>
      <div class="modal-body">
        
      	<form id="message-form">
      	  <div class="form-group">
		    <label for="subject">To</label>
		    <input type="text" class="form-control" id="to" name="to" placeholder="the recipient">
		  </div>
		  <div class="form-group">
		    <label for="subject">subject</label>
		    <input type="text" class="form-control" id="subject" name="subject" placeholder="the subject">
		  </div>
		  <div class="form-group">
		    <label for="message">message</label>
		    <textarea rows="10" class="form-control" id="message" name="message" placeholder="the message body"></textarea>
		  </div>
		  
		  <button type="submit" class="btn btn-default">Send</button>
		</form>

      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

@section('admin-content-box')
	
	<h3>Site Admins</h3>
	<hr>


	<table class="table table-hover table-bordered table-striped" id="admin-edit-table">
		<thead> 
			<tr> 
				<th>login ID</th> 
				<th>email</th> 
				<th>admin role</th> 
				<th>send message</th> 
				<th>active status</th> 
			</tr> 
		</thead>

		<tbody>
			@foreach($admins as $admin)
				@if($admin->name != Auth::user()->name)
				<tr>
					<td>
						<a 
						href=<?php $name = $admin->name;
								   echo route('admin.profile',['admin' => $name]);	
						 	 ?>
						>{{$admin->name}}
						</a>
					</td>
					<td>{{$admin->email}}</td>
					<td>{{$admin->roles[0]->name}}</td>
					<td><a class="btn btn-block btn-default message-btn" href="" data-id="{{$admin->name}}" data-toggle="modal" data-target="#myModal">Compose</a></td>

					@if($admin->roles[0]->name == 'su')
					<td></td>
					@elseif($admin->active == 1)
					<td><a class="btn btn-block btn-danger" 
						href=<?php $name = $admin->name;
								   echo route('admin.ban',['admin' => $name]);	
						 	 ?>
						>
						Ban
						</a>
					</td>
					@else
					<td><a class="btn btn-block btn-success" 
						href=<?php $name = $admin->name;
								   echo route('admin.activate',['admin' => $name]);	
						 	 ?>
						>
						Activate
						</a>
					</td>
					@endif

				</tr>
				@endif
			@endforeach
		</tbody>
	</table>

@endsection

@section('scripts')

	<script type="text/javascript">

	    $(document).ready(function(){
	        
	    	$('.message-btn').click(function(){
	    		$('#message-form #to').val($(this).data('id'));
	    	});

	    });

	</script>

@endsection