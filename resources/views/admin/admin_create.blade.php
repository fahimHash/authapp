@extends('admin')



@section('admin-content-box')
	<h3>New Admin</h3>
	<hr>
	<form class="" method="post" action="">

	  <div class="form-group">
	    <label for="username">login Id</label>
	    <input type="text" name="login_id" class="form-control" id="login Id" placeholder="login id">
	  </div>

	  <div class="form-group">
	    <label for="email">email</label>
	    <input type="email" name="email" class="form-control" id="email" placeholder="Email">
	  </div>
	  <div class="form-group">
	    <label for="password">Password</label>
	    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
	  </div>

	  <div class="form-group">
	    <label for="role">Assign a Role</label>
	  	<select id="role" class="form-control" name="role">
	  		@foreach($roles as $role)
	  		<option>{{$role->name}}</option>
	  		@endforeach
	  	</select>
	  </div>
	  {{ csrf_field() }}
	  <input type="submit" class="btn btn-default" value="Create">
	</form> 


@endsection
