<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/dashboard', 'AdminController@index')->name('admin.dashboard');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');


//ADMIN
Route::get('/admin/admin/create', 'AdminController@getCreatePage')->name('admin.create');
Route::post('/admin/admin/create', 'AdminController@create');

Route::get('/admin/admin/list', 'AdminController@getListPage')->name('admin.list');
Route::get('/admin/admin/ban/{admin}', 'AdminController@ban')->name('admin.ban');
Route::get('/admin/admin/activate/{admin}', 'AdminController@activate')->name('admin.activate');
Route::get('/admin/admin/profile/{admin}','AdminController@getProfilePage')->name('admin.profile');


//PERMISSIONS
Route::get('/admin/permission/create', 'PermissionController@getCreatePage')->name('permission.create');
Route::post('/admin/permission/create', 'PermissionController@create');

Route::get('/admin/permission/edit', 'PermissionController@getEditPage')->name('permission.edit');
Route::post('/admin/permission/roles/assign', 'PermissionController@assignToRoles')
      ->name('permission.roles.assign');
Route::post('/admin/permission/roles/revoke', 'PermissionController@revokeRoles')
      ->name('permission.roles.revoke');      



//ROLES
Route::get('/admin/roles/create', 'RoleController@getCreatePage')->name('role.create');           
Route::post('/admin/roles/create', 'RoleController@create');
Route::get('/admin/roles/list', 'RoleController@list')->name('role.list');       


//TEST
Route::get('/test',function(){
	\Artisan::call("routes");
	dd(\Artisan::output());
});
